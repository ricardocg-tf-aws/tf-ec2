
control "instance" do 
    impact 1 

    describe aws_ec2_instance(name: 'my-test-vm') do
        it { should exist }
    end
end