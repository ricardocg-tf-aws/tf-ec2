module "instance" {
    source                  = "../../../"
    instance_count          = 1
    name                    = "my-test-vm"
    ami                     = "ami-09d95fab7fff3776c"
    instance_type           = "t2.micro"
    key_name                = "key-mac"
}