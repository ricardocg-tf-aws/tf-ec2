output "id" {
  description = "List of IDs of instances"
  value       = module.instance.*.id
}

output "arn" {
  description = "List of ARNs of instances"
  value       = module.instance.*.arn
}

output "key_name" {
  description = "List of key names of instances"
  value       = module.instance.*.key_name
}

output "public_dns" {
  description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = module.instance.*.public_dns
}

output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = module.instance.*.public_ip
}

output "security_groups" {
  description = "List of associated security groups of instances"
  value       = module.instance.*.security_groups
}

output "tags" {
  description = "List of tags of instances"
  value       = module.instance.*.tags
}