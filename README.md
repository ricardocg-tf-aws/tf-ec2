# EC2 - WEB SERVER MODULE

- Deploys ec2 instance(s) with default web server installed it can be modified in user data variable

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| Security group ID | ID of the group created |

# Usage

  

