provider "aws" {
    region = "us-east-1"
}

data "aws_vpc" "default" {
  default = true
}

module "sg" {
    source          = "git::https://gitlab.com/ricardocg-tf-aws/tf-security-group.git?ref=master"
    vpc_id          = data.aws_vpc.default.id
    name            = "web-sg"
    description     = "SG for web ec2 instance"
}

resource "aws_instance" "main" {
    count                   = var.instance_count
    ami                     = var.ami
    instance_type           = var.instance_type
    user_data               = var.user_data
    key_name                = var.key_name
    vpc_security_group_ids  = ["${module.sg.sg_id}"]
    iam_instance_profile    = var.iam_instance_profile

    tags = {
        Environment = var.env
        Name        = var.name
    }
}
