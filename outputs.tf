output "id" {
  description = "List of IDs of instances"
  value       = aws_instance.main.*.id
}

output "arn" {
  description = "List of ARNs of instances"
  value       = aws_instance.main.*.arn
}

output "key_name" {
  description = "List of key names of instances"
  value       = aws_instance.main.*.key_name
}

output "public_dns" {
  description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.main.*.public_dns
}

output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_instance.main.*.public_ip
}

output "security_groups" {
  description = "List of associated security groups of instances"
  value       = aws_instance.main.*.security_groups
}

output "tags" {
  description = "List of tags of instances"
  value       = aws_instance.main.*.tags
}